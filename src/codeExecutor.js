const { Gio, GLib } = imports.gi;

import { GJSWrapper } from './gjsWrapper.js';

export class CodeExecutor {
    constructor() {
        this._subprocess = null;

        this._stdoutCallback = null;
        this._startCallback = null;
        this._stopCallback = null;
    }

    connectStdout(callback) {
        this._stdoutCallback = callback;
    }

    connectStart(callback) {
        this._startCallback = callback;
    }

    connectStop(callback) {
        this._stopCallback = callback;
    }

    disconnectStdout() {
        this._stdoutCallback = null;
    }

    disconnectStart() {
        this._startCallback = null;
    }

    disconnectStop() {
        this._stopCallback = null;
    }

    isRunning() {
        return this._subprocess !== null;
    }

    run(codeSnippet) {
        const argv = new GJSWrapper().tempScript(codeSnippet.getRaw()).toARGV();

        this._subprocess = new Gio.Subprocess({
            argv,
            flags: Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_MERGE
        });

        this._subprocess.init(null);

        const stdoutData = new Gio.DataInputStream({
            baseStream: this._subprocess.get_stdout_pipe()
        });

        const readLineAsyncCallback = (stream, result) => {
            try {
                const [line, len] = stream.read_line_finish_utf8(result);

                if (line !== null) {

                    if (this._stdoutCallback) {
                        this._stdoutCallback.call(this, line);
                    }

                    stream.read_line_async(GLib.PRIORITY_DEFAULT, null, readLineAsyncCallback);
                }
            } catch (e) {
                throw e;
            }
        };

        this._subprocess.wait_async(null, (source, result) => {
            let success = source.wait_finish(result);

            if (success) {
                this._subprocess = null; // TODO Should subprocess be null-ed here?
                stdoutData.read_line_async(GLib.PRIORITY_DEFAULT, null, readLineAsyncCallback);
            }

            if (this._stdoutCallback) {
                this._stopCallback.call(this, success);
            }
        });

        // TODO Double check this call.
        stdoutData.read_line_async(GLib.PRIORITY_DEFAULT, null, readLineAsyncCallback);

        this._startCallback.call(this);
    }

    stop() {
        this._subprocess.force_exit();
    }
};