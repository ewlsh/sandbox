/* window.js
 *
 * Copyright 2021 Evan Welsh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { CodeExecutor } from './codeExecutor.js';
import { CodeSnippet } from './codeSnippet.js';

import * as Util from './util.js';

const { GObject, Gio, Gtk, GtkSource } = imports.gi;

GObject.type_ensure(GtkSource.View);

export const SandboxWindow = GObject.registerClass({
    GTypeName: 'SandboxWindow',
    Template: 'resource:///guide/gjs/Sandbox/window.ui',
    InternalChildren: [
        'openFileButton',
        'executionControlButton',
        'executionControlStack',
        'clearButton',
        'headerBar',
        'sourceView',
        'outputTextView',
        'redoButton',
        'undoButton',
        'sourceBuffer',
        'outputBuffer'
    ]
}, class SandboxWindow extends Gtk.ApplicationWindow {
    _init(application) {
        super._init({
            application,
        });

        const languageManager = GtkSource.LanguageManager.get_default();
        const language = languageManager.get_language('js');

        log(languageManager.get_language_ids());

        this._sourceBuffer.set_highlight_syntax(true);
        this._outputBuffer.set_highlight_syntax(true);

        if (language) {
            this._sourceBuffer.set_language(language);
            this._outputBuffer.set_language(language);
        }


        this._codeExecutor = new CodeExecutor();

        this._codeExecutor.connectStdout((line) => {
            let iter = this._outputBuffer.get_end_iter();
            line = `${line}
`;
            this._outputBuffer.insert(iter, line, line.length);
        });

        this._codeExecutor.connectStart(() => {
            this._executionControlStack.set_visible_child_name('stop');
        });

        this._codeExecutor.connectStop((_success) => {
            this._executionControlStack.set_visible_child_name('run');
        });

        this._openFileButton.connect('clicked', (_widget, _data) => {
            const fileChooserDialog = new Gtk.FileChooserNative({
                title: 'Select a File',
                action: Gtk.FileChooserAction.OPEN,
                transient_for: this
            });

            // TODO restrict opening type

            const result = fileChooserDialog.run();

            if (result === Gtk.ResponseType.ACCEPT) {
                const contents = Util.loadContents(fileChooserDialog.get_filename());
                this._sourceBuffer.text = contents;
            }
        });

        this._executionControlButton.connect('clicked', (_widget, _data) => {
            if (!this._codeExecutor.isRunning()) {
                this.clearOutput();

                let code = this._sourceBuffer.text;

                this._codeExecutor.run(CodeSnippet.fromString(code));
            } else {
                this._codeExecutor.stop();
            }
        });

        this._clearButton.connect('clicked', (_widget, _data) => {
            if (this._codeExecutor.isRunning()) {
                this._codeExecutor.stop();
            }

            this.clear();
        });

        this._undoButton.connect('clicked', () => {
            this._sourceView.vfunc_undo();
        });

        this._redoButton.connect('clicked', () => {
            this._sourceView.vfunc_redo();
        });
    }

    clearOutput() {
        this._outputBuffer.text = '';
    }

    clear() {
        this._sourceBuffer.text = '';
        this.clearOutput();
    }

    saveTempScript(content) {
        const [file] = Gio.File.new_tmp(null);
        const [] = file.replace_contents(content, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);

        return file;
    }

    run() {
        const file = this.saveTempScript(this._source_buffer.text);
        this.runScript(file.get_path()).then(output => {
            this._output_buffer.text += output
        }).catch(err => {
            this._output_buffer.text += err.message;
        });
    }

});
