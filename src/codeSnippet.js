export class CodeSnippet {
    constructor(code) {
        this._code = code;
    }

    getRaw() {
        return this._code;
    }

    static fromString(string) {
        return new this(string);
    }
};